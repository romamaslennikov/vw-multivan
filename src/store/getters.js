const getters = {
  isPortrait: (state) => state.app.isPortrait,
  isMobile: (state) => state.app.isMobile,
  showMobileNav: (state) => state.app.showMobileNav,
  windowWidth: (state) => state.app.windowWidth,
  scrollY: (state) => state.app.scrollY,
  currentArticle: (state) => state.app.currentArticle,
  totalArticles: (state) => state.app.totalArticles,
  currentModel: (state) => state.app.currentModel,
};

export default getters;
