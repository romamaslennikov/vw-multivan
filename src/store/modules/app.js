/* eslint-disable */
import { contacts } from '@/api/app.js';

const app = {
  state: {
    isPortrait: null,
    isMobile: null,
    showMobileNav: null,
    windowWidth: null,
    scrollY: null,
    currentArticle: 1,
    totalArticles: 6,
    currentModel: 1,
  },
  mutations: {
    UPDATE_CURRENT_MODEL: (state, m) => {
      state.currentModel = m;
    },

    UPDATE_CURRENT_ARTICLE: (state, a) => {
      state.currentArticle = a;
    },

    UPDATE_IS_MOBILE: (state, isMobile) => {
      state.isMobile = isMobile;
    },

    SHOW_MOBILE_NAV: (state, show) => {
      state.showMobileNav = show;

      const body = document.querySelector('body');

      if (show) {
        body.classList.add('-lock');
      } else {
        body.classList.remove('-lock');
      }
    },

    UPDATE_IS_PORTRAIT: (state, isPortrait) => {
      state.isPortrait = isPortrait;
    },

    UPDATE_WINDOW_WIDTH(state, width) {
      state.windowWidth = width
    },

    UPDATE_WINDOW_SCROLL_Y(state, val) {
      state.scrollY = val
    },
  },
  actions: {
    ShowMobileNav: ({commit}, show) => {
      commit('SHOW_MOBILE_NAV', show);
    },

    SendContacts({ commit }, data) {
      return (async () => contacts(data))();
    },
  },
};

export default app;
