import qs from 'qs';
import request from '@/utils/request';

export function contacts(data) {
  const options = qs.stringify(data);

  return request({
    url: '/index.php',
    method: 'post',
    data: options,
  });
}

export default contacts;
