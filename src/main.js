import Vue from 'vue';
import Vue2TouchEvents from 'vue2-touch-events';
import VueHotkey from 'v-hotkey';
import VueScrollReveal from 'vue-scroll-reveal';
import modernizr from 'modernizr'; // eslint-disable-line
import Vuelidate from 'vuelidate';
import VueScrollTo from 'vue-scrollto';
import Meta from 'vue-meta';
import svg4everybody from 'svg4everybody';
import { VLazyImagePlugin } from 'v-lazy-image';
import App from './App.vue';
import router from './router';
import store from './store';
import './utils/filters';
import './utils/webp-support';
import { isIE11 } from './utils/device';

import 'normalize.css/normalize.css'; //  reset CSS
import '@/styles/layout.sass'; // global CSS

require('intersection-observer'); // for svg

if (isIE11) { // ie11 support
  require('es6-shim'); // eslint-disable-line
  require('element-closest-polyfill'); // eslint-disable-line
}

Vue.config.productionTip = false;

Vue.use(VueHotkey);
Vue.use(Vue2TouchEvents);
Vue.use(VueScrollReveal);
Vue.use(VueScrollTo);
Vue.use(Vuelidate);
Vue.use(Meta);
svg4everybody();
Vue.use(VLazyImagePlugin);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
