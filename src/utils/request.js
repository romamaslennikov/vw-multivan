import axios from 'axios';

const BASE_API = process.env.VUE_APP_NODE_ENV === 'production'
  ? process.env.VUE_APP_BASE_API : process.env.VUE_APP_BASE_API_DEV;

const service = axios.create({
  baseURL: BASE_API,
  timeout: 15000,
});

export { BASE_API };

export default service;
