import Vue from 'vue';
import modernizr from 'modernizr'; // eslint-disable-line

const isSafari = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;

Vue.prototype.$webp = !isSafari;

modernizr.on('webp', (result) => {
  if (!result) {
    Vue.prototype.$webp = false;
  }
});
